import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {FormlyModule, FormlyFieldConfig, FormlyBootstrapModule, Field, FieldWrapper} from '../../formly';
import {Declaratia310Service} from '../services/declaratieService';

@Component({
  selector: 'd600',
  styleUrls: ['./form.scss'],
  templateUrl: './form.html'
})
export class Form {

  form: FormGroup;
  userFields: FormlyFieldConfig[];
   response: string;
    pdfId: string;
    loading: boolean = false;
    butonGenerare: string = 'Generare';

  model: any = {
      "an": 2016,
      "adresa": {
          "strada": "",
          "nr": "",
          "etc": "..."
      },
  };

  constructor(fb: FormBuilder, private dService: Declaratia310Service) {
    this.form = fb.group({});

    this.userFields = [
      {
      className: 'col-xs-5 col-md-3',
      key: 'an',
      type: 'input',
      templateOptions: {
        label: "Anul",
        placeholder: "2016"
      }
    },
     {
      key: 'nume',
      type: 'input',
      templateOptions: {
        label: "Numele",
        placeholder: ""
      }

    },
     {
      key: 'iniTata',
      type: 'input',
      templateOptions: {
        label: "Initiala tatalui",
        placeholder: "4"
      }

    },
    {
      key: 'prenume',
      type: 'input',
      templateOptions: {
        label: "Prenumele",
        placeholder: "4"
      }},
      {
      key: 'str',
      type: 'input',
      templateOptions: {
        label: "Strada",
        placeholder: "4"
      }},
      {
      key: 'nr',
      type: 'input',
      templateOptions: {
        label: "Numar",
        placeholder: "4"
      }},
      {
      key: 'bl',
      type: 'input',
      templateOptions: {
        label: "Bloc",
        placeholder: "4"
      }},
      {
      key: 'sc',
      type: 'input',
      templateOptions: {
        label: "Scara",
        placeholder: "4"
      }},
      {
      key: 'et',
      type: 'input',
      templateOptions: {
        label: "Etaj",
        placeholder: "4"
      }},
      {
      key: 'ap',
      type: 'input',
      templateOptions: {
        label: "Ap.",
        placeholder: "4"
      }},
      {
      key: 'judSect',
      type: 'input',
      templateOptions: {
        label: "Judet/Sector",
        placeholder: "4"
      }},
      {
      key: 'local',
      type: 'input',
      templateOptions: {
        label: "Localitate",
        placeholder: "4"
      }},
      {
      key: 'codPost',
      type: 'input',
      templateOptions: {
        label: "Cod postal",
        placeholder: "4"
      }},
      {
      key: 'tel',
      type: 'input',
      templateOptions: {
        label: "Telefon",
        placeholder: "4"
      }},
      {
      key: 'fax',
      type: 'input',
      templateOptions: {
        label: "Fax",
        placeholder: "4"
      }},
      {
      key: 'email',
      type: 'input',
      templateOptions: {
        label: "E-mail",
        placeholder: "4"
      }},
      {
      key: 'cnp',
      type: 'input',
      templateOptions: {
        label: "Cod numeric personal",
        placeholder: "4"
      }},
      {
      key: 'banca',
      type: 'input',
      templateOptions: {
        label: "Banca",
        placeholder: "4"
      }},
      {
      key: 'contBanc',
      type: 'input',
      templateOptions: {
        label: "Cont bancar (IBAN)",
        placeholder: "4"
      }}, 
      {
            className: 'col-xs-12',
            type: 'radio',
            key: 'temei',
            templateOptions: {
                options: [
                  {
                    key: 1,
   value: 'Optez pentru aplicarea cotei integrale de contribuție de asigurări sociale corespunzătoare condițiilor normale de muncă, potrivit art.151 alin.(5) dinCodul fiscal'
                    
                }, {
                    key: 2,
 value: '- Renunț la opțiunea pentru aplicarea cotei integrale de contribuție de asigurări sociale corespunzătoare condițiilor normale de muncă, potrivit art.151alin din Codul fiscal'
                }],
                label: ' OPȚIUNE PRIVIND COTA DE CONTRIBUȚIE DE ASIGURĂRI SOCIALE() Pentru anul în curs, la stabilirea obligațiilor de plată cu titlu contribuție de asigurări sociale:',
            }
        },
      {
      key: 'numPreDen2',
      type: 'input',
      templateOptions: {
        label: "Nume, prenume/Denumire",
        placeholder: "4"
      }},
      {
      key: 'str2',
      type: 'input',
      templateOptions: {
        label: "Strada",
        placeholder: "4"
      }},
      {
      key: 'judSec2',
      type: 'input',
      templateOptions: {
        label: "Judet/Sector",
        placeholder: "4"
      }},
      {
      key: 'local2',
      type: 'input',
      templateOptions: {
        label: "Localitate",
        placeholder: "4"
      }},
      {
      key: 'codP2',
      type: 'input',
      templateOptions: {
        label: "Cod postal",
        placeholder: "4"
      }},
      {
      key: 'tel2',
      type: 'input',
      templateOptions: {
        label: "Telefon",
        placeholder: "4"
      }},
      {
      key: 'fax2',
      type: 'input',
      templateOptions: {
        label: "Fax",
        placeholder: "4"
      }},
      {
      key: 'email2',
      type: 'input',
      templateOptions: {
        label: "E-mail",
        placeholder: "4"
      }},
      {
      key: 'nrInreg',
      type: 'input',
      templateOptions: {
        label: "Nr. Inregistrare",
        placeholder: "4"
      }},
      {
      key: 'data',
      type: 'input',
      templateOptions: {
        label: "Data",
        placeholder: "4"
      }}


    ]
  }

  postProcess () {
    this.loading = true;
    this.butonGenerare = 'Regenerare';
    this.response = '';


    this.dService.sendData(this.model).then(response => {
    this.loading = false;
    this.response = JSON.parse(response["_body"]);
    
    if(this.response['fileId'] != '') {
    this.pdfId = this.response['fileId']
       }
    })
  }
}
